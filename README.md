Installing the Dooba SDK
====================

The simplest way to install the SDK is with this single command:

`
curl -s https://bitbucket.org/dooba_core/sdk/raw/master/install.sh | bash -l
`

This will require the following:

- bash
- ruby
- grep
- git
- tar

**Note**

There is no need to be root or do 'sudo'.

The script will install the Dooba SDK in the current user's home, under the '.dooba' directory.

If you prefer, you may also clone the repository yourself, then simply run **./install.sh**.
