#!/usr/bin/env bash

# Dooba SDK
# SDK Installer

# SDK Source
sdk_src='https://git@bitbucket.org/dooba_core/sdk'

# Setup Root
root_name='.dooba'
root_path=~/"${root_name}"

# Get User
user=`whoami`

# Colors
rst="\e[39m"
dbla="\e[30m"
dred="\e[31m"
dgre="\e[32m"
dyel="\e[33m"
dblu="\e[34m"
dmag="\e[35m"
dcya="\e[36m"
dwhi="\e[37m"
bbla="\e[90m"
bred="\e[91m"
bgre="\e[92m"
byel="\e[93m"
bblu="\e[94m"
bmag="\e[95m"
bcya="\e[96m"
bwhi="\e[97m"

# Inform
echo -e "\n${byel}Hello there!${rst}\nThis script will install the Dooba SDK for your user: ${bgre}${user}${rst}.\n"
echo -e "    -> Source:   ${bmag}${sdk_src}${rst}"
echo -e "    -> Target:   ${bmag}${root_path}${rst}"
echo ''

# Spinner Data
spin="/-\\|"
spin_size=4
spin_idx=0

# Print Spinner
function spin()
{
	local text=$1
	local prog=$2
	local t=$text
	local c=`echo $spin | cut -c$[1 + spin_idx]`

	# Throttle speed
	sleep 0.05

	# Get Progress
	if [ "${prog}" != "" ]
	then
		local pt=`tail -n 1 "${prog}" | sed s/'\r'/'\n'/g | sed s/'[ \t]*$'/''/g | tail -n 1`
		local t="${text} [${bmag}${pt}${rst}]"
	fi

	# Print Spinner
	echo -en "\r\033[2K${t} ${c}"

	# Loop
	spin_idx=$[(spin_idx + 1) % spin_size]
}

# Wait for process
function pwait()
{
	local pid=$1
	local text=$2
	local prog=$3
	local is_done=0

	# Loop while process is running
	while [ $is_done -eq 0 ]
	do
		# Print Spinner
		spin "${text}" "${prog}"

		# Check Process
		kill -0 $pid &>/dev/null; is_done=$?
	done

	# Done
	echo -e "\r\033[2K${text} Done!"
}

# Run Bash command and wait
function bwait()
{
	local rvar=$1
	local rlog=$2
	local cmd=$3
	local text=$4
	local ld="`date +%s.%N`-$$"
	local ltbase="/tmp/dooba-${ld}"
	local llog="${ltbase}.log"

	# Run Command, Track output and Exit value (all in background)
	bash -c "bash -c \"$cmd\" &>> \"${llog}\"; echo \$? > \"${ltbase}\""&

	# Wait for process
	pwait $! "${text}" "${llog}"

	# Return Exit Value & Log File
	local lres=`cat ${ltbase}`
	eval $rvar="'$lres'"
	eval $rlog="'$llog'"
}

# Acquire Clean Root
bwait res log "rm -rfv \"${root_path}\"" " [${bgre}+${rst}] Cleaning install path..."
if [ $res -ne 0 ]; then echo -e " [${bred}!${rst}] ERROR: Failed to clean install path. Check log file for details: ${dred}${log}${rst}"; exit 1; fi

# Clone SDK
bwait res log "git clone --verbose --progress \"${sdk_src}\" \"${root_path}\"" " [${bgre}+${rst}] Fetching from source..."
if [ $res -ne 0 ]; then echo -e " [${bred}!${rst}] ERROR: Failed to get Dooba SDK from server. Check log file for details: ${dred}${log}${rst}"; exit 1; fi

# Extract Toolchain
bwait res log "tar xzfv \"${root_path}/toolchain/avr8.tgz\" -C \"${root_path}/toolchain\"" " [${bgre}+${rst}] Extracting toolchain..."
if [ $res -ne 0 ]; then echo -e " [${bred}!${rst}] ERROR: Failed to extract toolchain archive. Check log file for details: ${dred}${log}${rst}"; exit 1; fi

# Ensure Binary Directory is in Path
echo -en " [${bgre}+${rst}] Adding SDK to path..."
if [ -e ~/.profile ]; then grep -v 'PATH="$PATH:$HOME/'"${root_name}"'/bin"' ~/.profile > ~/.profile.tmp.dooba; mv ~/.profile.tmp.dooba ~/.profile; fi
echo "export PATH=\"\$PATH:\$HOME/${root_name}/bin\"" >> ~/.profile
echo ' Done!'

# Ensure Toolchain is in Path
echo -en " [${bgre}+${rst}] Adding toolchain to path..."
if [ -e ~/.profile ]; then grep -v 'PATH="$PATH:$HOME/'"${root_name}"'/toolchain/avr8/bin"' ~/.profile > ~/.profile.tmp.dooba; mv ~/.profile.tmp.dooba ~/.profile; fi
echo "export PATH=\"\$PATH:\$HOME/${root_name}/toolchain/avr8/bin\"" >> ~/.profile
echo ' Done!'

# Inform
echo -e "\n${bgre}All done!${rst}\n\nThe Dooba SDK has been installed in \"${dyel}~/${root_name}${rst}\".\nYour shell profile has been updated.\nYou need to spawn a new shell or use the following command to refresh this one:\n${dgre}source ~/.profile${rst}\n\nThank you for choosing Dooba :D\n - Eresse <info@dooba.io>\n"
