#!/usr/bin/env ruby

# Dooba SDK
# Common features

# Disable warnings
$VERBOSE = nil

# Includes
require 'yaml'
require 'json'
require 'pathname'
require 'fileutils'
require 'digest/md5'

# Ensure external dependencies are available
def req_deps deps
	deps.each do |d|
		begin
			require d
		rescue Exception
			puts "Looks like you're missing [#{d}] - Installing now..."
			`gem install #{d}`; proc = $?
			raise "ERROR: Installation failed for #{d} - Please install #{d} manually" unless proc.success?
			Gem.clear_paths
			require d
		end
	end
end

# Require external dependencies
req_deps %w(aromat spiffup git rest-client)

# Default Source
DEFAULT_SOURCE_BASE = 'bitbucket.org/dooba_core/'

# Core Repos
CORE_BASE = 'https://api.bitbucket.org/2.0/repositories/dooba_core'

# Default Protocol
DEFAULT_PROTOCOL = 'https://'

# Default User
DEFAULT_USER = 'dooba'

# Default Branch
DEFAULT_BRANCH = 'master'

# Default Baudrate
DEFAULT_BAUDRATE = 19200

# Default Serial Port
DEFAULT_SER_PORT = '/dev/ttyUSB0'

# Acquire SDK Root
SDK_ROOT = File.dirname(Pathname.new(File.expand_path(__FILE__)).realpath).to_s

# Paths
SRC_PATH = 'src'
OUT_PATH = 'out/obj'
LIB_PATH = 'out/lib'
BIN_PATH = 'out/bin'

# Spinner
SPINNER = { idx: 0, pts: ['/', '-', '\\', '|'] }

# Spin
def spin txt = nil
	print "\r"
	print SPINNER[:pts][SPINNER[:idx]]
	print ' '
	print txt if txt
	$stdout.flush
	SPINNER[:idx] = (SPINNER[:idx] + 1) % SPINNER[:pts].size
end

# Clear Spin
def spin_clr
	print "\r        \r"
	$stdout.flush
end

# Spin in background
def spin_background txt = nil
	spin_background_stop
	$stop_spinner = false
	$spinner_thread = Thread.new { (spin txt; sleep 0.01) until $stop_spinner; spin_clr }
end

# Stop background spinner
def spin_background_stop
	$stop_spinner = true
	$spinner_thread.join if $spinner_thread
	$spinner_thread = nil
end

# Get Command Name
def cmd_name default_name
	CMD_NAME rescue default_name
end

# Print Error
def print_err opts, e
	puts e
	e.backtrace.each { |x| puts "    -> #{x}" } if opts[:verbose] > 1
end

# Trap Errors
def trap_errs opts

	# Begin
	begin

		# Yield
		yield if block_given?

	# Handle Exceptions
	rescue Exception => e

		# Print
		print_err opts, e

		# Error
		exit 1
	end
end

# Update
def update opts

	# Inform
	puts " #{"[ # ]".rpad(5).bcyan} Updating SDK..." if opts[:verbose] > 0

	# Update SDK
	g = Git.open SDK_ROOT
	d = g.fetch('origin') || ''
	d << "\n" << g.pull
	puts d if opts[:verbose] > 1
end

# Fetch list of Core repositories
def get_core_repos fields = nil

	# Prepare repos
	repos = []

	# Prepare URL
	url_to_call = URI.parse CORE_BASE
	url_to_call.query = URI.encode_www_form fields: "#{(fields << 'next').join(',')}" if fields

	# Loop through pages
	response_hash = { next: url_to_call.to_s }
	while response_hash[:next]

		# Get next page of repos
		response_hash = JSON.parse(RestClient.get(response_hash[:next]).try(:body)).sym_keys
		repos.concat response_hash[:values] if response_hash[:values]
	end

	repos
end

# Project Configuration Class
class ProjConf

	# Element Types
	TYPES = %w(app lib)

	# Element Configuration File Name
	FILE_NAME = 'dfe.conf'

	# Constructor
	def initialize path = nil, name: nil, type: nil, opts: {}

		# Check Arguments
		raise "ERROR: Cannot create an element configuration without at least a path or a name & type" unless path || (name && type)
		raise "ERROR: Invalid type [#{type}] for element configuration" if name && !(TYPES.include?(type.to_s))

		# Load Project Definition
		@hash = name ? { name: name, type: type }.merge(opts.sym_keys) : (YAML::load_file(ProjConf.gen_path(path)) || {}).sym_keys
		@hash[:path] = path

		# Clean up dependencies
		@hash[:deps] = (@hash[:deps] || []).collect do |d|

			# Check Type
			case d
				when String
					{ name: d.to_s }
				when Hash
					{ name: d.first.first.to_s }.merge d.first.last
				else
					raise "ERROR: Invalid dependency structure in [#{"#{p}/dfe.conf".red}]"
			end
		end

		# Fixup Frequency if loading from a file
		if @hash[:freq] && !name
			if /([0-9]+)(([KMG])(Hz)?|(UL)?)/i =~ @hash[:freq].to_s
				@hash[:freq] = "#{$1.to_i * { nil => 1, K: 1000, M: 1000000, G: 1000000000 }[$3.try(:to_sym).try(:upcase)]}UL"
			end
		end
	end

	# Path
	def self.gen_path directory
		"#{directory}/#{FILE_NAME}"
	end

	# Array Access
	def [] k
		@hash[k.to_sym]
	end

	# Array Assign
	def []= k, v
		@hash[k.to_sym] = v
	end

	# Write to File
	def write file = nil
		raise "ERROR: No file specified for writing element configuration" unless file || @hash[:path]
		file ||= ProjConf.gen_path @hash[:path]
		@hash[:deps] = @hash[:deps].collect { |d| d[:name] } if @hash[:deps]
		File.write file, @hash
			.select { |k, v| k != :path }						# Remove path
			.reject { |k, v| k == :deps && v.try(:empty?) }		# Remove deps if empty
			.str_keys											# Stringify keys
			.to_yaml											# Convert to YAML
			.gsub(/^---\n/, '')									# Strip head
			.gsub(/^( *- )/, '  \1')							# Indent arrays
	end
end
