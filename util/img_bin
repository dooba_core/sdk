#!/usr/bin/env ruby

# Dooba SDK
# Image Conversion Utility

# Disable warnings
$VERBOSE = nil

# External Includes
require 'pathname'
require "#{File.dirname(File.dirname(Pathname.new(File.expand_path(__FILE__)).realpath))}/common.rb"

# Require external dependencies
req_deps %w(aromat spiffup)

# Usage
def usage
	puts "Usage: img_bin <IN_DATA_FILE>"
	exit 1
end

# Acquire Args
usage unless ARGV.size == 1
idf = ARGV[0]

# Check File Format
m = /.+\.([0-9]+)\.([0-9]+)\.img$/.match idf
unless m
	puts "Invalid file name structure: #{idf}"
	exit 1
end

# Acquire Size
width = m[1].to_i
height = m[2].to_i
rsize = File.read(idf).bytes.size

# Extract Path & Name
path = File.dirname idf
name = File.basename(idf).gsub(/\.img$/, '').gsub(/\.[0-9]+\.[0-9]+$/, '')

# Read Input Data File
img_data = File.read(idf).bytes.reject.with_index { |e, i| (i % 4) == 3 }

# Prepare Output
out = []

# Write Image Size & Font Size
out << (width & 0xff)
out << ((width >> 8) & 0xff)
out << (height & 0xff)
out << ((height >> 8) & 0xff)

# Pack up Pixels
(width * height).times do |idx|
	vr = img_data[(idx * 3) + 0] >> 3
	vg = img_data[(idx * 3) + 1] >> 2
	vb = img_data[(idx * 3) + 2] >> 3
	v16 = (vr << 11) | (vg << 5) | vb
	hi = (v16 >> 8) & 0xff
	lo = v16 & 0xff
	out << lo
	out << hi
end

# Write Output
File.write "#{path}/#{name}.bin", out.pack('C*')
